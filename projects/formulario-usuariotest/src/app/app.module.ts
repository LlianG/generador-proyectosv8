import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormularioUsuarioModule } from 'formulario-usuario'
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormularioUsuarioModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
