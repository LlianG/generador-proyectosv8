/*
 * Public API Surface of formulario-usuario
 */

export * from './lib/formulario-usuario.service';
export * from './lib/formulario-usuario.component';
export * from './lib/formulario-usuario.module';
