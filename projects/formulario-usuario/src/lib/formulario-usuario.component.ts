import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormularioUsuario } from './formulario-usuario.class';

@Component({
  selector: 'ng-formulario-usuario',
  templateUrl: './formulario-usuario.component.html',
  styles: []
})
export class FormularioUsuarioComponent implements OnInit {
  @Input() set _usuario(val){
    console.log(val)
  }
  @Output() EventUsuario = new EventEmitter();

  formData: FormularioUsuario;

  constructor() {
    this.formData = new FormularioUsuario()
  }

  ngOnInit() {

  }
  enviar(){
    this.EventUsuario.emit({mensaje: 'Ya lo pase:'+ this.formData.nombre})
  }

}
