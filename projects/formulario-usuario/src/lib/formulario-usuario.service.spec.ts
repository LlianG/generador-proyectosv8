import { TestBed } from '@angular/core/testing';

import { FormularioUsuarioService } from './formulario-usuario.service';

describe('FormularioUsuarioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FormularioUsuarioService = TestBed.get(FormularioUsuarioService);
    expect(service).toBeTruthy();
  });
});
