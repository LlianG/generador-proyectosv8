import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { FormularioUsuarioComponent } from './formulario-usuario.component';
import { CommonModule } from '@angular/common'
import { HttpClientModule } from '@angular/common/http'

@NgModule({
  declarations: [FormularioUsuarioComponent],
  imports: [
    FormsModule,
    CommonModule,
    HttpClientModule
  ],
  exports: [FormularioUsuarioComponent]
})
export class FormularioUsuarioModule { }
